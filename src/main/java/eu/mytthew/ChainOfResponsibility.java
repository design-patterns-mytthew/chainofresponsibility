package eu.mytthew;

import eu.mytthew.shape.*;

public class ChainOfResponsibility {
	public static void main(String[] args) {
		Shape triangle = new Triangle();
		Shape square = new Square();
		Shape trapeze = new Trapeze();
		Element squareElement = new SquareElement();
		Element triangleElement = new TriangleElement();
		Element trapezeElement = new TrapezeElement();
		squareElement.setNext(triangleElement);
		triangleElement.setNext(trapezeElement);
		squareElement.detect(trapeze);
	}
}

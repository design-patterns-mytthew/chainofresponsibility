package eu.mytthew.shape;

public class TriangleElement extends Element {
	@Override
	public void detect(Shape shape) {
		if (shape instanceof Triangle) {
			System.out.println("it's triangle!");
			System.out.println(((Triangle) shape).countArea(1, 3));
		} else {
			next.detect(shape);
		}
	}
}

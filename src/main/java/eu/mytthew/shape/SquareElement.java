package eu.mytthew.shape;

public class SquareElement extends Element {
	@Override
	public void detect(Shape shape) {
		if (shape instanceof Square) {
			System.out.println("It's a square!");
			System.out.println(((Square) shape).countArea(12));
		} else {
			next.detect(shape);
		}
	}
}

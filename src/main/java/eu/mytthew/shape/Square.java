package eu.mytthew.shape;

public class Square implements Shape {
	public double countArea(double a) {
		return a * a;
	}
}

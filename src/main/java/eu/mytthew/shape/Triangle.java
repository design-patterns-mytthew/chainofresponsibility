package eu.mytthew.shape;

public class Triangle implements Shape {
	public double countArea(double a, double h) {
		return a * h / 2;
	}
}

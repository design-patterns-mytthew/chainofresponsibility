package eu.mytthew.shape;

public class TrapezeElement extends Element {
	@Override
	public void detect(Shape shape) {
		if (shape instanceof Trapeze) {
			System.out.println("It's a trapeze!");
			System.out.println(((Trapeze) shape).countArea(1, 2, 1));
		} else {
			next.detect(shape);
		}
	}
}

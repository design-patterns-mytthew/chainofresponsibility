package eu.mytthew.shape;

public abstract class Element {
	protected Element next;

	public void setNext(Element next) {
		this.next = next;
	}

	public abstract void detect(Shape shape);
}
